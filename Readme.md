# Approach

For architecture we use the following methods:
   
## Clean Architecture
This is the base for complex applications, I decided to use this, to showcase my abilities
using SOLID and separation of concerns.

## MVVM
This is basic for any related UI application. It is interchangeable with MVI (which I prefer) and 
we could use UI components to delegate UI logic into them and use the Fragments as controllers
as seen in https://netflixtechblog.com/making-our-android-studio-apps-reactive-with-ui-components-redux-5e37aac3b244

## Modularization
The modularization in this project is a feature based modularization. It's not dynamic-feature 
modularization due to the dependency injection library constraints used in this demo, but it could
be solved given time

## Dependency Injection
The dependency injection used in this project is Koin. This is because it can be used in Kotlin 
Multiplatform projects instead of Hilt and Dagger, which are Android based.

## Git-flow
As repository and CI is concerned, this project uses git flow (just two features, which I apologise for)
as is easily integrated with Agile methodologies, allowing us to track specific features, bug, fixes, etc...

# Third party libraries

## Ktor
The reason for using this instead of OKHttp + Retrofit is the same as with Koin, it allows us to use
this in Kotlin pure projects, and with no JVM or Android dependencies, which is neat.

## Kotlin-serialization
The kotlin serialization library is a personal pick of mine. It has no real world advantages over Gson,
but it's easy to use and to implement

## Coroutines
In this project I use the Flow api from coroutines. It's super similar to the RX pattern but, simpler
and easier. Right now RX is on decline and people are moving to MVI architectures notifying states
instead of raw data to the UI (which is preferable) and Kotlin Flow allow us to do this smoothtly

# TODOS

## UI improvements
The UI in this project is practically a scaffold. I tend to use my time showcasing my abilities in
the logic and domain department instead of focusing on the UI, but it's very much improvable.

## Orientation handling
This is the most important aspect about UI. This should be done by using the SavedStateHandle that
comes now with the ViewModels which is much easier done now with Hilt.

## Testing
This project clearly lacks Unit Testing for its repository and unit cases, also it needs Network tests
for Ktor, and UI Tests with Espresso.

## Dynamic features
As I said before, the go to now is to write new applications with dynamic feature modules, which
allows to have even more modularization in the application, but for that we would have to use 
Hilt with Dagger instead of Koin. If we still want to use Koin, we could have our dependencies to
load lazy but it's a bit more complicated.

## Navigation
Single Activity multiple Fragments applications are way more manageable than old multiple Activity
applications, and the Navigation library helps us a lot in that. Right now it fits perfectly with
dynamic feature applications and not so much with library feature applications and we can't use its
full potential

