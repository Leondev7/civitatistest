
object Versions {
    //GLOBAL
    const val gradle = "4.1.3"
    const val kotlin = "1.4.32"

    //THREETEN
    const val threeten = "1.3.0"

    //COROUTINES
    const val coroutinesAndroid = "1.4.3"

    //SERIALIZATION
    const val kotlinxSerialization = "1.0.0"

    //ANDROID
    const val appcompat = "1.2.0"
    const val material = "1.1.0"
    const val fragment = "1.2.5"
    const val coreKTX = "1.2.0"
    const val constraintLayout = "1.1.3"
    const val legacy = "1.0.0"
    const val lifecycle= "2.3.1"
    const val navigation = "2.3.5"
    const val recyclerView = "1.1.0"

    //KTOR
    const val ktor = "1.5.2"

    //KOIN
    const val koin = "3.0.1"

    //GLIDE
    const val glide = "4.11.0"


}

object BuildConfig {
    const val APPLICATION_ID = "com.leondev7.civitatistest"

    const val BUILD_TOOLS_VERSION = "30.0.1"
    const val COMPILE_SDK_VERSION = 30
    const val MIN_SDK_VERSION = 21
    const val TARGET_SDK_VERSION = COMPILE_SDK_VERSION

    const val VERSION_CODE = 1
    const val VERSION_NAME = "1.0"

    const val SUPPORT_LIBRARY_VECTOR_DRAWABLES = true

    const val TEST_INSTRUMENTATION_RUNNER = "androidx.test.runner.AndroidJUnitRunner"

}


object Android{
    const val appcompatX = "androidx.appcompat:appcompat:${Versions.appcompat}"
    const val materialX = "com.google.android.material:material:${Versions.material}"
    const val fragmentX = "androidx.fragment:fragment-ktx:${Versions.fragment}"
    const val coreKTX = "androidx.core:core-ktx:${Versions.coreKTX}"
    const val constraintLayoutX = "androidx.constraintlayout:constraintlayout:${Versions.constraintLayout}"
    const val livedataX = "androidx.lifecycle:lifecycle-livedata-ktx:${Versions.lifecycle}"
    const val viewmodelX = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.lifecycle}"
    const val recyclerView = "androidx.recyclerview:recyclerview:${Versions.recyclerView}"
    const val navigation = "androidx.navigation:navigation-ui-ktx:${Versions.navigation}"
}


object Navigation{
    const val navFragment = "androidx.navigation:navigation-fragment-ktx:${Versions.navigation}"
    const val navUI = "androidx.navigation:navigation-fragment-ktx:${Versions.navigation}"
}

object Ktor{
    val clientCore = "io.ktor:ktor-client-core:${Versions.ktor}"
    val clientJson = "io.ktor:ktor-client-json:${Versions.ktor}"
    val clientLogging = "io.ktor:ktor-client-logging:${Versions.ktor}"
    val clientSerialization = "io.ktor:ktor-client-serialization:${Versions.ktor}"
    val clientCio = "io.ktor:ktor-client-cio:${Versions.ktor}"
}
object Datetime{
    val threeten = "com.jakewharton.threetenabp:threetenabp:${Versions.threeten}"
}

object Kotlin{
    val stdlib = "org.jetbrains.kotlin:kotlin-stdlib:${Versions.kotlin}"
}
object Koin{
    val android = "io.insert-koin:koin-android:${Versions.koin}"
}

object Serialization{
    const val core = "org.jetbrains.kotlinx:kotlinx-serialization-core:${Versions.kotlinxSerialization}"
    const val json = "org.jetbrains.kotlinx:kotlinx-serialization-json:${Versions.kotlinxSerialization}"
}

object BuildPlugins{
    const val gradle = "com.android.tools.build:gradle:${Versions.gradle}"
    const val kotlin = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlin}"
    const val kotlinxSerialization = "org.jetbrains.kotlin:kotlin-serialization:${Versions.kotlin}"

}

object Glide{
    const val glide = "com.github.bumptech.glide:glide:${Versions.glide}"
}



