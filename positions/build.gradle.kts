plugins {
    id("com.android.library")
    kotlin("android")
    kotlin("android.extensions")
    id("kotlin-android")
}
android {
    compileSdkVersion(BuildConfig.COMPILE_SDK_VERSION)
    buildToolsVersion = BuildConfig.BUILD_TOOLS_VERSION

    defaultConfig {
        minSdkVersion(BuildConfig.MIN_SDK_VERSION)
        targetSdkVersion(BuildConfig.TARGET_SDK_VERSION)
        versionCode = BuildConfig.VERSION_CODE
        versionName = BuildConfig.VERSION_NAME
        testInstrumentationRunner = BuildConfig.TEST_INSTRUMENTATION_RUNNER
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    lintOptions {
        isWarningsAsErrors = true
        isAbortOnError = true
    }

    kotlinOptions {
        jvmTarget = "1.8"
    }
}

dependencies {
    implementation (project(":core"))
    implementation (Kotlin.stdlib)
    //Android
    implementation (Android.coreKTX)
    implementation (Android.materialX)
    implementation (Android.appcompatX)
    implementation (Android.constraintLayoutX)
    implementation (Android.livedataX)
    implementation (Android.viewmodelX)
    implementation (Android.recyclerView)
    //Glide
    implementation (Glide.glide)
    //DateTime
    implementation (Datetime.threeten)
    //Koin
    implementation(Koin.android)
    //Navigation
    //Navigation
    implementation(Navigation.navFragment)
    implementation(Navigation.navUI)
}