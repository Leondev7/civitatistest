package com.leondev7.feature.positions.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import com.leondev7.civitatistest.core.extensions.findNavController
import com.leondev7.feature.positions.R


class WebViewFragment : Fragment() {



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_web_view, container, false)

        val webView :WebView= view.findViewById(R.id.webView)
        webView.webViewClient = WebViewClient()

        val url = arguments?.getString("companyUrl")
        url?.let { webView.loadUrl(it) }
        return view
    }

    private fun routeToWebView(companyUrl: String){
        val bundle = bundleOf("companyUrl" to companyUrl)
        findNavController().navigate(R.id.action_positionFragment_to_webViewFragment, bundle)
    }


}