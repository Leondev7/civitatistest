package com.leondev7.feature.positions.di

import com.leondev7.feature.positions.data.PositionsRepository
import com.leondev7.feature.positions.domain.IPositionsRepository
import com.leondev7.feature.positions.domain.interactor.GetPositions
import com.leondev7.feature.positions.domain.interactor.GetPositionsByDate
import com.leondev7.feature.positions.domain.interactor.GetPositionsByDateFiltered
import com.leondev7.feature.positions.presentation.PositionsListViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val positionsModule = module {

    single<IPositionsRepository> { PositionsRepository(get()) }
    factory { GetPositions(get()) }
    factory { GetPositionsByDate(get()) }
    factory { GetPositionsByDateFiltered(get()) }

    viewModel { PositionsListViewModel(get()) }
}