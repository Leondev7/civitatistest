package com.leondev7.feature.positions.domain.interactor

import com.leondev7.feature.positions.domain.Position


class GetPositionsByDate
constructor(
    private val getPositions : GetPositions
){
    suspend operator fun invoke() : List<Position>{

        return getPositions().sortedByDescending { it.date }
    }

}
