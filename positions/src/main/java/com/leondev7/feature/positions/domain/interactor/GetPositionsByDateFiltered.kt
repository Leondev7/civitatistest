package com.leondev7.feature.positions.domain.interactor

import com.leondev7.feature.positions.domain.Position
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter
import java.text.SimpleDateFormat
import java.util.*

class GetPositionsByDateFiltered
constructor(
    private val getPositionsByDate: GetPositionsByDate
){

    private val dateFormatter = DateTimeFormatter.ofPattern("EEE MMM dd HH:mm:ss z yyyy", Locale.ENGLISH)
    suspend operator fun invoke() : Flow<List<Position>> {
        val currentDate = LocalDate.now()
        return flowOf(getPositionsByDate().filter { position ->

            val positionDate = LocalDate.parse(position.date,dateFormatter)
            currentDate <= positionDate.plusDays(5)
        })
    }

}
