package com.leondev7.feature.positions.domain.interactor

import com.leondev7.feature.positions.domain.Position
import com.leondev7.feature.positions.domain.IPositionsRepository

class GetPositions
constructor(
        private val repository: IPositionsRepository
){
    suspend operator fun invoke() : List<Position> =
        repository.getPositions()
}

