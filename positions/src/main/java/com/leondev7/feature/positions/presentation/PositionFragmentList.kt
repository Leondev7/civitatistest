package com.leondev7.feature.positions.presentation

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import com.leondev7.civitatistest.core.extensions.findNavController
import com.leondev7.feature.positions.R
import com.leondev7.feature.positions.domain.Position
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * A fragment representing a list of Items.
 */
class PositionFragmentList : Fragment() {

    val viewModel : PositionsListViewModel by viewModel()
    private lateinit var positionRecycler: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_position_list, container, false)

        positionRecycler = view.findViewById(R.id.list_positions)
        initAdapter()
        return view
    }

    private fun initAdapter(){
        positionRecycler.layoutManager = LinearLayoutManager(context)

        observeData()
    }

    private fun observeData(){
        viewModel.positions.observe(viewLifecycleOwner, {
            positionRecycler.adapter = MyPositionRecyclerViewAdapter(it){ model->
                routeToSingle(model)
            }

        })
        viewModel.getPositionsByDateFiltered()
    }



    private fun routeToSingle( position : Position){
        val bundle = bundleOf("position" to position)
        findNavController().navigate(R.id.action_positionListFragment_to_positionFragment,bundle)

    }


}