package com.leondev7.feature.positions.domain


interface IPositionsRepository {

    suspend fun getPositions() : List<Position>
}