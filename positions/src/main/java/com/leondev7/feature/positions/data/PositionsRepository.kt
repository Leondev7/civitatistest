package com.leondev7.feature.positions.data

import com.leondev7.civitatistest.core.network.CivitatisApi
import com.leondev7.feature.positions.domain.IPositionsRepository
import com.leondev7.feature.positions.domain.Position

class PositionsRepository
    constructor(private val api: CivitatisApi): IPositionsRepository {

    override suspend fun getPositions(): List<Position> {
        return api.getPositions().map { githubResponse ->  
            Position(
                title =githubResponse.title,
                companyName = githubResponse.company,
                location = githubResponse.location,
                type = githubResponse.type,
                date = githubResponse.date,
                companyLogo = githubResponse.companyLogo?:"",
                description = githubResponse.description,
                companyUrl = githubResponse.companyURL
            )
        }
    }
}