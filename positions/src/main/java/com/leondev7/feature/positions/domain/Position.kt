package com.leondev7.feature.positions.domain

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Position
constructor(
    val title: String,
    val companyName: String,
    val location : String,
    val type : String,
    val date : String,
    val companyLogo : String,
    val description : String,
    val companyUrl : String
) : Parcelable
