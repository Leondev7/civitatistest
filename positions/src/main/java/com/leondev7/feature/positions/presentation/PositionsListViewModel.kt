package com.leondev7.feature.positions.presentation

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.leondev7.feature.positions.domain.Position
import com.leondev7.feature.positions.domain.interactor.GetPositionsByDateFiltered
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class PositionsListViewModel
constructor(
    private val getPositions : GetPositionsByDateFiltered
): ViewModel() {

    private val _positions = MutableLiveData<List<Position>>()
    val positions: LiveData<List<Position>> = _positions

    fun getPositionsByDateFiltered(){
        viewModelScope.launch {
            getPositions().collect {positions ->
                _positions.postValue(positions)
            }
        }
    }
}