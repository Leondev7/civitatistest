package com.leondev7.feature.positions.presentation

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.leondev7.feature.positions.R
import com.leondev7.feature.positions.domain.Position


class MyPositionRecyclerViewAdapter(
    private val positions: List<Position>,
    private val onItemClicked: (Position) -> Unit
) : RecyclerView.Adapter<MyPositionRecyclerViewAdapter.ViewHolder>() {

    
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_position, parent, false)
        return ViewHolder(view){
            onItemClicked(positions[it])
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = positions[position]
        holder.txtTitle.text = item.title
        holder.txtCompanyName.text = item.companyName
        holder.txtLocation.text = item.location
        holder.txtType.text = item.type
        holder.txtDate.text = item.date

        Glide.with(holder.imgCompany.context)
            .load(item.companyLogo)
            .into(holder.imgCompany)
    }

    override fun getItemCount(): Int = positions.size

    inner class ViewHolder(view: View,  onItemClicked: (Int) -> Unit) : RecyclerView.ViewHolder(view) {
        init {
            itemView.setOnClickListener {
                onItemClicked(adapterPosition)
            }
        }
        val txtTitle: TextView = view.findViewById(R.id.txt_title)
        val txtCompanyName: TextView = view.findViewById(R.id.txt_company_name)
        val txtLocation: TextView = view.findViewById(R.id.txt_location)
        val txtType: TextView = view.findViewById(R.id.txt_type)
        val txtDate: TextView = view.findViewById(R.id.txt_date)
        val imgCompany: ImageView = view.findViewById(R.id.img_company)


    }

}