package com.leondev7.feature.positions.presentation

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.text.Html
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.os.bundleOf
import com.bumptech.glide.Glide
import com.leondev7.civitatistest.core.extensions.findNavController
import com.leondev7.feature.positions.R
import com.leondev7.feature.positions.domain.Position

class PositionFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.position_fragment, container, false)

        val txtTitle: TextView = view.findViewById(R.id.txt_title)
        val txtCompanyName: TextView = view.findViewById(R.id.txt_company_name)
        val txtLocation: TextView = view.findViewById(R.id.txt_location)
        val txtType: TextView = view.findViewById(R.id.txt_type)
        val txtDate: TextView = view.findViewById(R.id.txt_date)
        val txtCompanyUrl : TextView = view.findViewById(R.id.txt_company_url)
        val txtDescription : TextView = view.findViewById(R.id.txt_description)
        val imgCompany: ImageView = view.findViewById(R.id.img_company)

        val position = arguments?.getParcelable<Position>("position")

        txtTitle.text = position?.title
        txtCompanyName.text = position?.companyName
        txtLocation.text = position?.location
        txtType.text = position?.type
        txtDate.text = position?.date
        txtCompanyUrl.text = position?.companyUrl
        txtDescription.text = Html.fromHtml(position?.description)

        txtCompanyUrl.setOnClickListener {
            position?.companyUrl?.let { it1 -> routeToWebView(it1) }
        }
        Glide.with(imgCompany.context)
            .load(position?.companyLogo)
            .into(imgCompany)
        return view
    }

    private fun routeToWebView(companyUrl :String){
        val bundle = bundleOf("companyUrl" to companyUrl)
        findNavController().navigate(R.id.action_positionFragment_to_webViewFragment,bundle)
    }

}