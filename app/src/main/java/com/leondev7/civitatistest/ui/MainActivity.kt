package com.leondev7.civitatistest.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.leondev7.civitatistest.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
    }
}