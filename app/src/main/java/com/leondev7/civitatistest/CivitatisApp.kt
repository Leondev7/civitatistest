package com.leondev7.civitatistest

import android.app.Application
import com.jakewharton.threetenabp.AndroidThreeTen
import com.leondev7.civitatistest.core.di.coreModule
import com.leondev7.feature.positions.di.positionsModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class CivitatisApp : Application(){

    override fun onCreate() {
        super.onCreate()
        AndroidThreeTen.init(this);

        startKoin{
            androidLogger()
            androidContext(this@CivitatisApp)
            modules(listOf(coreModule, positionsModule))
        }

    }


}
