plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("android.extensions")
}

android {
    compileSdkVersion(BuildConfig.COMPILE_SDK_VERSION)
    buildToolsVersion = BuildConfig.BUILD_TOOLS_VERSION
    defaultConfig {
        applicationId = BuildConfig.APPLICATION_ID
        minSdkVersion(BuildConfig.MIN_SDK_VERSION)
        targetSdkVersion(BuildConfig.TARGET_SDK_VERSION)
        versionCode = BuildConfig.VERSION_CODE
        versionName = BuildConfig.VERSION_NAME
        testInstrumentationRunner = BuildConfig.TEST_INSTRUMENTATION_RUNNER
    }
    packagingOptions {
        exclude("META-INF/*.kotlin_module")
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    lintOptions {
        isWarningsAsErrors = true
        isAbortOnError = true
    }

    kotlinOptions {
        jvmTarget = "1.8"
    }
}

dependencies {

    implementation(project(":positions"))
    implementation(project(":core"))
    implementation (Kotlin.stdlib)
    //Android
    implementation (Android.coreKTX)
    implementation (Android.materialX)
    implementation (Android.appcompatX)
    implementation (Android.constraintLayoutX)
    implementation (Android.livedataX)
    implementation (Android.viewmodelX)
    //DateTime
    implementation (Datetime.threeten)
    //Koin
    implementation(Koin.android)
    //Navigation
    implementation(Navigation.navFragment)
    implementation(Navigation.navUI)


}

