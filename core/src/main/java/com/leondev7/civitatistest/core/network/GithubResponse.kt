package com.leondev7.civitatistest.core.network

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
@Serializable
data class GithubResponse(
    @SerialName("id")
    val id: String,
    @SerialName("type")
    val type : String,
    @SerialName("url")
    val url : String,
    @SerialName("created_at")
    val date : String,
    @SerialName("company")
    val company : String,
    @SerialName("company_url")
    val companyURL : String,
    @SerialName("location")
    val location : String,
    @SerialName("title")
    val title : String,
    @SerialName("description")
    val description : String,
    @SerialName("how_to_apply")
    val howToApply : String,
    @SerialName("company_logo")
    val companyLogo : String?,
   )

