package com.leondev7.civitatistest.core.di

import com.leondev7.civitatistest.core.network.CivitatisApi
import org.koin.dsl.module

class CoreModule {
}

val coreModule = module {

    single { CivitatisApi() }
}