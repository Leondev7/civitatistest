package com.leondev7.civitatistest.core.network

import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.features.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.features.logging.*
import io.ktor.client.request.*

class CivitatisApi {
    private val httpClient = HttpClient(CIO) {
        install(JsonFeature) {
            serializer = KotlinxSerializer()
        }
        install(Logging) {
            logger = Logger.DEFAULT
            level = LogLevel.BODY
        }


        HttpResponseValidator {
            validateResponse { response ->
                val statusCode = response.status.value
                when (statusCode) {
                    in 300..399 -> throw RedirectResponseException(response,"Cached response text")
                    in 400..499 -> throw ClientRequestException(response,"Cached response text")
                    in 500..599 -> throw ServerResponseException(response)
                }

                if (statusCode >= 600) {
                    throw ResponseException(response,"Cached response text")
                }
            }
        }
    }

    suspend fun getPositions(
    ) : List<GithubResponse> {
        val finalUrl = baseUrl+ positionsEndpoint
        return httpClient.get {
            url(finalUrl)
        }
    }


    companion object {
        private const val baseUrl = "https://jobs.github.com"
        private const val positionsEndpoint = "/positions.json"
    }


}

